import React from 'react';

export class Input extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {value: this.props.value};
		
		this.handleChanged = this.handleChanged.bind(this);
	}
	
	handleChanged(event) {
		this.setState({value: event.target.value});
	}
	
	render() {
		return (
			<input type="text" placeholder={this.props.placeholder} value={this.state.value} onChange={this.handleChanged} readOnly={this.props.readOnly}/>
		);
	}
}