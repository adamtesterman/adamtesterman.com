import React from 'react';
import { Input } from './Input';
import { Select } from './Select';
import { TextArea } from './TextArea';

export class App extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = { 
			alignment: "Lawful Good",
			background: "Acolyte",
			characterClass: "Paladin",
			expToNext: "900",
			level: "2nd",
			profBonus: "2",
			race: "Half-Elf"
		};
		
		this.handleUserChangedCharacterClass = this.handleUserChangedCharacterClass.bind(this);
		this.handleUserChangedLevel = this.handleUserChangedLevel.bind(this);
		this.handleUserChangedRace = this.handleUserChangedRace.bind(this);
	}
	
	handleUserChangedCharacterClass(newClass) {
		this.setState({
			characterClass: newClass
		})
	}
	
	handleUserChangedLevel(newLevel) {
		let expToNext;
		let profBonus;
		
		switch(newLevel) {
			case "1st":
				expToNext = "300";
				profBonus = "2";
				break;
			case "2nd":
				expToNext = "900";
				profBonus = "2";
				break;
			case "3rd":
				expToNext = "2,700";
				profBonus = "2";
				break;
			case "4th":
				expToNext = "6,500";
				profBonus = "2";
				break;
			case "5th":
				expToNext = "14,000";
				profBonus = "3";
				break;
			case "6th":
				expToNext = "23,000";
				profBonus = "3";
				break;
			case "7th":
				expToNext = "34,000";
				profBonus = "3";
				break;
			case "8th":
				expToNext = "48,000";
				profBonus = "3";
				break;
			case "9th":
				expToNext = "64,000";
				profBonus = "4";
				break;
			case "10th":
				expToNext = "85,000";
				profBonus = "4";
				break;
			case "11th":
				expToNext = "100,000";
				profBonus = "4";
				break;
			case "12th":
				expToNext = "120,000";
				profBonus = "4";
				break;
			case "13th":
				expToNext = "140,000";
				profBonus = "5";
				break;
			case "14th":
				expToNext = "165,000";
				profBonus = "5";
				break;
			case "15th":
				expToNext = "195,000";
				profBonus = "5";
				break;
			case "16th":
				expToNext = "225,000";
				profBonus = "5";
				break;
			case "17th":
				expToNext = "265,000";
				profBonus = "6";
				break;
			case "18th":
				expToNext = "305,000";
				profBonus = "6";
				break;
			case "19th":
				expToNext = "355,000";
				profBonus = "6";
				break;
			case "20th":
				expToNext = "0";
				profBonus = "6";
				break;
		}
		
		this.setState({
			expToNext: expToNext,
			profBonus: profBonus,
			level: newLevel
		})
	}
	
	handleUserChangedRace(newRace) {
		this.setState({
			race: newRace
		});
	}
	
	render() {
		return (
			<div>
				<div id="header">
					<div>
						<Input placeholder="Character Name" value="Vynathias Chateau Par'thiel" />
						<h2>{this.state.level} Level {this.state.race} {this.state.characterClass}</h2>
					</div>
				</div>
				<nav>
					<div>
						<a href="#profile">Profile</a>
						<a href="#stats">Stats</a>
						<a href="#inventory">Inventory</a>
						<a href="#spells">Spells</a>
					</div>
				</nav>
				<h1 id="profile">Profile</h1>
				<div className="content">
					<div className="panel">
						<img id="portrait" src="images/profile.png" />
					</div>
					<div className="panel list">
						<h3>Age</h3>
						<Input placeholder="21" value="20" />
						<h3>Race</h3>
						<Select 
							defaultValue={this.state.race}
							onChange={this.handleUserChangedRace}
							values={[
								"Dark Elf", 
								"Dragonborn", 
								"Forest Gnome", 
								"Gnome", 
								"Half-Elf", 
								"High Elf", 
								"Hill Dwarf", 
								"Human", 
								"Lightfoot", 
								"Mountain Dwarf", 
								"Rock Gnome", 
								"Stout", 
								"Tiefling", 
								"Wood Elf"]}
							labels={[
								"Dark Elf (Drow)", 
								"Dragonborn", 
								"Forest Gnome", 
								"Gnome", 
								"Half-Elf", 
								"High Elf", 
								"Hill Dwarf", 
								"Human", 
								"Lightfoot", 
								"Mountain Dwarf", 
								"Rock Gnome", 
								"Stout", 
								"Tiefling", 
								"Wood Elf"]}
						/>
						<h3>Class</h3>
						<Select 
							defaultValue={this.state.characterClass}
							onChange={this.handleUserChangedCharacterClass}
							values={[
								"Barbarian", 
								"Bard", 
								"Cleric", 
								"Druid", 
								"Fighter", 
								"Monk", 
								"Paladin", 
								"Ranger", 
								"Rogue", 
								"Sorcerer", 
								"Warlock", 
								"Wizard"]}
							labels={[
								"Barbarian", 
								"Bard", 
								"Cleric", 
								"Druid", 
								"Fighter", 
								"Monk", 
								"Paladin", 
								"Ranger", 
								"Rogue", 
								"Sorcerer", 
								"Warlock", 
								"Wizard"]}
						/>
						<h3>Alignment</h3>
						<Select 
							defaultValue={this.state.alignment}
							values={[
								"Chaotic Evil", 
								"Chaotic Good", 
								"Chaotic Neutral", 
								"Lawful Evil", 
								"Lawful Good", 
								"Lawful Neutral", 
								"Neutral Evil", 
								"Neutral Good", 
								"Neutral"]}
							labels={[
								"Chaotic Evil", 
								"Chaotic Good", 
								"Chaotic Neutral", 
								"Lawful Evil", 
								"Lawful Good", 
								"Lawful Neutral", 
								"Neutral Evil", 
								"Neutral Good", 
								"Neutral"]}
						/>
						<h3>Background</h3>
						<Select 
							defaultValue={this.state.background}
							values={[
								"Acolyte", 
								"Charlatan", 
								"Criminal", 
								"Entertainer", 
								"Folk Hero", 
								"Guild Artisan", 
								"Hermit", 
								"Noble", 
								"Outlander", 
								"Sage", 
								"Sailor", 
								"Soldier",
								"Urchin"]}
							labels={[
								"Acolyte", 
								"Charlatan", 
								"Criminal", 
								"Entertainer", 
								"Folk Hero", 
								"Guild Artisan", 
								"Hermit", 
								"Noble", 
								"Outlander", 
								"Sage", 
								"Sailor", 
								"Soldier",
								"Urchin"]}
						/>
						<h3>Skin</h3>
						<Input placeholder="Fair" value="Fair" />
						<h3>Eyes</h3>
						<Input placeholder="Brown" value="Green" />
						<h3>Hair</h3>
						<Input placeholder="Brown" value="Brown" />
						<h3>Height</h3>
						<Input placeholder="5&apos;10&quot;" value="5&apos;10&quot;" />
						<h3>Weight</h3>
						<Input placeholder="150lbs" value="178lbs" />
					</div>
					<div className="panel">
						<h3>Character Backstory</h3>
						<TextArea value="Vynathias was born to Aratil Par'thiel and Lady Liza Chateau. He was conceived as a result of the events after the battle with Loon'cuh'hablayse. His father chose to go into stasis instead of Liza going back. Vynathias grew up living with his mother in the chapel of the Order of Majere in Majere city.
At Aratil's request, Liza raised Vynathias in the Yorved faith so that he could one day become a paladin and so that he would be strong enough to take down the unknown forces that caused his mother to go into stasis the first time. Vyn started training to be a paladin at the age of 15. Five years later, he completed his training." />
					</div>
					<div className="panel">
						<h3>Personality Traits</h3>
						<TextArea value="Likes to be called Vyn for short.
Child at heart.
Protective of everyone close to him, especially his Mother." />
					</div>
					<div className="panel">
						<h3>Ideals</h3>
						<TextArea value="Honor
Faith
Nation
Greater Good" />
					</div>
					<div className="panel">
						<h3>Bonds</h3>
						<TextArea value="Order of the Majere
Majere City" />
					</div>
					<div className="panel">
						<h3>Flaws</h3>
						<TextArea value="Can be overconfident." />
					</div>
					<div className="panel">
						<h3>Allies and Organizations</h3>
						<TextArea value="Lady Liza Chateau (mother)
Priestess Ellisandre
Silvara of the Curinost
The Order of Majere
Magnis, The Grand Wizard of Majere
Arik, The Grand Warrior of Majere
Grimple, son of Grizz and Giz'rah"/>
					</div>
				</div>
				<h1 id="stats">Stats</h1>
				<div className="content">
					<div className="panel list">
						<h3>Level</h3>
						<Select 
							defaultValue={this.state.level}
							onChange={this.handleUserChangedLevel}
							values={[
								"1st", 
								"2nd", 
								"3rd", 
								"4th", 
								"5th",
								"6th",
								"7th",
								"8th",
								"9th",
								"10th",
								"11th",
								"12th",
								"13th",
								"14th",
								"15th",
								"16th",
								"17th",
								"18th",
								"19th",
								"20th"]}
							labels={[
								"1st", 
								"2nd", 
								"3rd", 
								"4th", 
								"5th",
								"6th",
								"7th",
								"8th",
								"9th",
								"10th",
								"11th",
								"12th",
								"13th",
								"14th",
								"15th",
								"16th",
								"17th",
								"18th",
								"19th",
								"20th"]}
						/>
						<h3>Experience</h3>
						<div style={{width: "50%"}}><Input placeholder="0" value="0" /><input value={" / " + this.state.expToNext} readOnly={true} /></div>
						<h3>Hit Points</h3>
						<Input placeholder="0" value="24 / 24" />
						<h3>Armor Class</h3>
						<Input placeholder="0" value="18" />
						<h3>Speed</h3>
						<Input placeholder="0" value="30" />
						<h3>Initiative</h3>
						<Input placeholder="0" value="0" />
						<h3>Proficiency</h3>
						<input placeholder="0" value={this.state.profBonus} readOnly={true} />
						<h3>Passive Wis.</h3>
						<Input placeholder="0" value="9" />
						<h3>Hit Dice</h3>
						<Input placeholder="1d6 / 1d6" value="2d10 / 2d10" />
						<h3>Death Saves</h3>
						<Input placeholder="0 / 0" value="0 / 0" />
					</div>
					<div className="panel">
						<h3>Proficiencies and Languages</h3>
						<TextArea value="Common
Dwarven
Elvish
Heavy Armor
Light Armor
Martial Weapons
Medium Armor
Orc
Shields
Simple Weapons"/>
					</div>
					<div className="panel table">
						<h3>Ability</h3>
						<h3>Score</h3>
						<h3>Modifier</h3>
						<h3>Saving Throw</h3>
						<h3>Strength</h3>
						<Input placeholder="0" value="15" />
						<Input placeholder="0" value="2" />
						<Input placeholder="0" value="2" />
						<h3>Dexterity</h3>
						<Input placeholder="0" value="12" />
						<Input placeholder="0" value="1" />
						<Input placeholder="0" value="1" />
						<h3>Constitution</h3>
						<Input placeholder="0" value="14" />
						<Input placeholder="0" value="2" />
						<Input placeholder="0" value="2" />
						<h3>Intelligence</h3>
						<Input placeholder="0" value="10" />
						<Input placeholder="0" value="0" />
						<Input placeholder="0" value="0" />
						<h3>Wisdom*</h3>
						<Input placeholder="0" value="9" />
						<Input placeholder="0" value="-1" />
						<Input placeholder="0" value="1" />
						<h3>Charisma*</h3>
						<Input placeholder="0" value="17" />
						<Input placeholder="0" value="3" />
						<Input placeholder="0" value="5" />
					</div>
					<div className="panel double">
						<h3>Acrobatics</h3>
						<Input placeholder="0" value="1" />
						<h3>Animal Handling</h3>
						<Input placeholder="0" value="-1" />
						<h3>Arcana</h3>
						<Input placeholder="0" value="0" />
						<h3>Athletics</h3>
						<Input placeholder="0" value="2" />
						<h3>Deception</h3>
						<Input placeholder="0" value="3" />
						<h3>History</h3>
						<Input placeholder="0" value="0" />
						<h3>Insight</h3>
						<Input placeholder="0" value="-1" />
						<h3>Intimidation</h3>
						<Input placeholder="0" value="3" />
						<h3>Investigation</h3>
						<Input placeholder="0" value="0" />
						<h3>Medicine</h3>
						<Input placeholder="0" value="-1" />
						<h3>Nature</h3>
						<Input placeholder="0" value="0" />
						<h3>Perception</h3>
						<Input placeholder="0" value="-1" />
						<h3>Performance</h3>
						<Input placeholder="0" value="3" />
						<h3>Persuasion*</h3>
						<Input placeholder="0" value="5" />
						<h3>Religion*</h3>
						<Input placeholder="0" value="2" />
						<h3>Sleight of Hand</h3>
						<Input placeholder="0" value="1" />
						<h3>Stealth</h3>
						<Input placeholder="0" value="1" />
						<h3>Survival</h3>
						<Input placeholder="0" value="-1" />
					</div>
					<div className="panel">
						<h3>Features and Traits</h3>
						<TextArea value="Divine Sense
Divine Smite
Great Weapon Fighting
Lay on Hands
Spellcasting"/>
					</div>
				</div>
				<h1 id="inventory">Inventory</h1>
				<div className="content">
					<div className="panel list">
						<h3>Platinum</h3>
						<Input placeholder="0" value="0" />
						<h3>Electrum</h3>
						<Input placeholder="0" value="0" />
						<h3>Gold</h3>
						<Input placeholder="0" value="0" />
						<h3>Silver</h3>
						<Input placeholder="0" value="0" />
						<h3>Copper</h3>
						<Input placeholder="0" value="0" />
					</div>
					<div className="panel">
						<h3>Equipment</h3>
						<TextArea value="Greatsword
Amulet of Turn Undead
Set of common clothes and boots
Full plate armor
Tabard of the Order of Majere
Symbol of the Order of Majere (tabard inscription)
Prayer book
Belt pouch
Hair tie
Winter furs and snowshoes" />
					</div>
					<div className="panel">
						<h3>Treasure</h3>
						<TextArea value="5 sticks of incense" />
					</div>
				</div>
				<h1 id="spells">Spells</h1>
				<div className="content">
					<div className="panel">
						<h3>Cantrips</h3>
						<TextArea value="N/A" />
					</div>
					<div className="panel">
						<h3>1st Level</h3>
						<TextArea value="Cure Wounds
Detect Good and Evil" />
					</div>
					<div className="panel">
						<h3>2nd Level</h3>
						<TextArea value="" />
					</div>
					<div className="panel">
						<h3>3rd Level</h3>
						<TextArea value="" />
					</div>
					<div className="panel">
						<h3>4th Level</h3>
						<TextArea value="" />
					</div>
					<div className="panel">
						<h3>5th Level</h3>
						<TextArea value="" />
					</div>
					<div className="panel">
						<h3>6th Level</h3>
						<TextArea value="" />
					</div>
					<div className="panel">
						<h3>7th Level</h3>
						<TextArea value="" />
					</div>
					<div className="panel">
						<h3>8th Level</h3>
						<TextArea value="" />
					</div>
					<div className="panel">
						<h3>9th Level</h3>
						<TextArea value="" />
					</div>
				</div>
			</div>
		);
	}
}