import React from 'react';

export class Select extends React.Component {
	constructor(props) {
		super(props);
		
		this.handleChanged = this.handleChanged.bind(this);
	}
	
	handleChanged(event) {
		this.props.onChange(event.target.value);
	}
	
	render() {
		const options = [];
		for (let i = 0; i < this.props.values.length; i++) {
			options.push(<option key={i.toString()} value={this.props.values[i]}>{this.props.values[i]}</option>);
		}
		
		return (
			<select
				defaultValue={this.props.defaultValue}
				onChange={this.handleChanged}>
				{options}
			</select>
		);
	}
}