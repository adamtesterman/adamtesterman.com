import React from 'react';

export class TextArea extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {value: this.props.value};
		
		this.handleChanged = this.handleChanged.bind(this);
	}
	
	handleChanged(event) {
		this.setState({value: event.target.value});
	}
	
	render() {
		return (
			<textarea value={this.state.value} onChange={this.handleChanged} />
		);
	}
}