var com;
(function (com) {
    var jestermen;
    (function (jestermen) {
        var grandPrixGranny;
        (function (grandPrixGranny) {
            var NPC = (function () {
                //=================================
                //		SETUP
                //=================================
                function NPC() {
                    //CONSTANTS
                    this.MIN_DY = 3.5;
                    this.MAX_DY = 4.5;
                    this.ACCELERATION_Y = .01;
                    //PRIMITIVES
                    this._dx = 0;
                    this._dy = 0;
                    this.CreateView();
                    this._collisionBox = new createjs.Rectangle(0, 0, 60, 80); //positions updated later
                }
                NPC.prototype.CreateView = function () {
                    var viewSpriteSheetData = {
                        images: [grandPrixGranny.AssetManager.getAssetByID("npc")],
                        frames: { width: 80, height: 110 }
                    };
                    var viewSpriteSheet = new createjs.SpriteSheet(viewSpriteSheetData);
                    this._view = new createjs.Sprite(viewSpriteSheet);
                    this._view.gotoAndStop(0);
                };
                NPC.prototype.reset = function () {
                    this._dx = 0;
                    this._dy = Math.random() * (this.MAX_DY - this.MIN_DY) + this.MIN_DY;
                    var rand = Math.floor(Math.random() * 3);
                    this._view.gotoAndStop(rand);
                };
                NPC.prototype.update = function () {
                    //UPDATE DY
                    if (this._isAccelerating) {
                        this._dy += this.ACCELERATION_Y;
                    }
                    else {
                        this._dy -= this.ACCELERATION_Y;
                    }
                    //Bounds check dy
                    if (this._dy > this.MAX_DY) {
                        this._dy = this.MAX_DY;
                        this._isAccelerating = false;
                    }
                    else if (this._dy < this.MIN_DY) {
                        this._dy = this.MIN_DY;
                        this._isAccelerating = true;
                    }
                    //UPDATE VIEW
                    this._view.x += this._dx;
                    this._view.y += this._dy;
                    //UPDATE COLLISION BOX
                    this._collisionBox.x = this._view.x + 10;
                    this._collisionBox.y = this._view.y + 15;
                };
                Object.defineProperty(NPC.prototype, "collisionBox", {
                    //=================================
                    //		ACCESSORS
                    //=================================
                    get: function () {
                        return this._collisionBox;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(NPC.prototype, "view", {
                    get: function () {
                        return this._view;
                    },
                    enumerable: true,
                    configurable: true
                });
                return NPC;
            })();
            grandPrixGranny.NPC = NPC;
        })(grandPrixGranny = jestermen.grandPrixGranny || (jestermen.grandPrixGranny = {}));
    })(jestermen = com.jestermen || (com.jestermen = {}));
})(com || (com = {}));
//# sourceMappingURL=NPC.js.map